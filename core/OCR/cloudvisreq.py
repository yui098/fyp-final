from base64 import b64encode
from os import makedirs
import json
import requests
import cv2
import numpy as np
from util import PIL2CV, CV2PIL
from skimage.transform import radon
from matplotlib.mlab import rms_flat
import imutils
from multiprocessing.pool import ThreadPool

ENDPOINT_URL = 'https://vision.googleapis.com/v1/images:annotate'
RESULTS_DIR = 'jsons'
makedirs(RESULTS_DIR, exist_ok=True)

class ocrApi:
    def __init__(self, api_key):
        self.api_key = str(api_key)
        # My self
        # self.api_key = "AIzaSyDGrHXS-2IY6PL3clmAWYdoV3jQa7brYIc"
        # simon
        # self.api_key = "AIzaSyBvXj3axy_VR_Gp5UqgBYh8kpspvw8fy-g"
        # Sing
        # self.api_key = "AIzaSyDNSe - wXhZXJIvi - M9tZyv4Aayekz5RJdg"

    def make_image_data_list(self, image_filenames):
        """
        image_filenames is a list of filename strings
        Returns a list of dicts formatted as the Vision API
            needs them to be
        """
        img_requests = []
        for imgname in image_filenames:
            with open(imgname, 'rb') as f:
                ctxt = b64encode(f.read()).decode()
                img_requests.append({
                    'image': {'content': ctxt},
                    'features': [{
                        'type': 'TEXT_DETECTION',
                        'maxResults': 1
                    }]
                })
        return img_requests

    def make_image_data(self, image_data):
        """Returns the image data lists as bytes"""
        if isinstance(image_data, (str, list)):
            imgdict = self.make_image_data_list(image_data)
        else:
            imgdict = self.make_image(image_data)
        return json.dumps({"requests": imgdict}).encode()

    def request_ocr(self, api_key, img_input):
        response = requests.post(ENDPOINT_URL,
                                 data=self.make_image_data(img_input),
                                 params={'key': api_key},
                                 headers={'Content-Type': 'application/json'})
        return response

    def rotate(self, in_image):
        try:
            in_image = CV2PIL(in_image)
        except:
            pass
        image = np.asarray(in_image.convert('L'))
        I = image - np.mean(image)  # Demean; make the brightness extend above and below zero
        sinogram = radon(I)
        r = np.array([rms_flat(line) for line in sinogram.transpose()])
        rotation = np.argmax(r)
        degree = abs(90 - rotation)
        if (degree <= 20):
            #print("rotate :" + str(degree))
            image = imutils.rotate(image, 90 - rotation)
            #print(type(image))
        return image

    def calculate_brightness(self, image):
        try:
            image = CV2PIL(image)
        except:
            pass
        greyscale_image = image.convert('L')
        histogram = greyscale_image.histogram()
        pixels = sum(histogram)
        brightness = scale = len(histogram)

        for index in range(0, scale):
            ratio = histogram[index] / pixels
            brightness += ratio * (-scale + index)

        return 1 if brightness == 255 else brightness / scale

    def make_image(self, input_image):
        img_requests = []
        _bright = self.calculate_brightness(input_image)
        if _bright < 0.3:
            pool2 = ThreadPool(processes=4)
            image = pool2.apply_async(self.rotate, (input_image,)).get()
        else:
            image = PIL2CV(input_image)
        _, buffer = cv2.imencode('.png', image)
        con_txt = b64encode(buffer).decode()
        img_requests.append({
            'image': {'content': con_txt},
            'features': [{
                'type': 'TEXT_DETECTION',
                'maxResults': 1
            }]
        })
        return img_requests

    def google_ocr(self, image):
        ans = None

        response = self.request_ocr(self.api_key, image)
        if response.status_code != 200 or response.json().get('error'):
            print(response.text)
        else:
            for idx, resp in enumerate(response.json()['responses']):
                try:
                    receive = resp['textAnnotations'][0]
                    ans = receive['description']
                except:
                    pass
        return ans


if __name__ == "__main__":
    filename = ['C:/Users/Tcc/Desktop/CPR_Model_demo_2/license/166.png']
    ans = google_ocr(filename)
    print(ans)
