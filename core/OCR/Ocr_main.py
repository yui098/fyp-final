from collections import Counter
from PIL import ImageOps
from difflib import SequenceMatcher
from multiprocessing.pool import ThreadPool
from .cloudvisreq import ocrApi
from core.Yolo.algo import connentDatabase
from util import PIL2CV, CV2PIL
import math
import pytesseract
import cv2
import numpy as np
import re
import datetime

__author__ = "Jonathan Tam"
__copyright__ = ""
__credits__ = ["Jonathan Tam", "Ben Ng", "John Po", "Ernest Ng"]
__version__ = "0.0.1"
__maintainer__ = "Jonathan Tam"
__email__ = "tamchungchak@gmail.com"

pytesseract.pytesseract.tesseract_cmd = 'C://Program Files (x86)/Tesseract-OCR/tesseract.exe'

class Ocr_main:

    def __init__(self, parent, database, ui, api_key, debugMode=False):
        self.parent = parent
        self.pool = ThreadPool(processes=12)
        self.ocrApi = ocrApi(api_key)
        self.debugMode = debugMode
        self.listresultF = []
        self.listresultB = []
        self.listcartype = []
        self.connentDatabase = connentDatabase(self, ui, database)
        self.nextCarFront = False
        self.nextCarBack = False

    def long_slice(self, original_img, slice_size):
        # need to use PIL format
        """slice an image into parts slice_size tall"""
        width, height = original_img.size
        upper = 0
        left = 0
        slices = int(math.ceil(height / slice_size))
        count = 1
        image_f = []
        for slice in range(slices):
            # if we are at the end, set the lower bound to be the bottom of the image
            lower = int(count * slice_size)
            bbox = (left, upper, width, int(lower))
            working_slice = original_img.crop(bbox)
            image_f.append(working_slice)
            # save the slice
            # working_slice.save(os.path.join(outdir,out_name + str(count) +".png"))
            upper += slice_size
            count += 1
        return image_f


    def add_border(self, input_image, border, color=0):
        if isinstance(border, int) or isinstance(border, tuple):
            bimg = ImageOps.expand(input_image, border=border, fill=color)
        else:
            raise RuntimeError('Border is not an integer or tuple!')
        # bimg.save(output_image)
        return bimg

    def similar(self, list):
        if len(list) > 0:
            compare = list[0]
            for index in range(len(list)-1):
                sim = SequenceMatcher(None, compare, list[index+1]).ratio()
                if sim > 0.8:
                    if len(list[index+1]) > len(compare) & len(list[index+1]) < 6:
                        compare = list[index+1]
        else:
            compare = None
        return compare

    def resize_img(self, unresize_img):
        # need to use cv2 format
        try:
            unresize_img = PIL2CV(unresize_img)
        except:
            pass
        resized_image = cv2.resize(unresize_img, (400, 300))
        resized_image = CV2PIL(resized_image)
        return resized_image


    def tessoract_ocr(self, tessor_img):
        if isinstance(tessor_img, np.ndarray):
            print(type(tessor_img))
            tessor_img = CV2PIL(tessor_img)
        tessor_img = tessor_img.convert('L')
        tessored_result = pytesseract.image_to_string(tessor_img)
        # print("tessoract_ocr: "+ result+"\n")
        return tessored_result

    # For two row car plate
    def dou_line_ocr(self, mark_img):
        test = ""
        dou_line_result = ""
        count = 0
        uppercheck = []
        lowercheck = []
        # resize the image
        rimg = self.resize_img(mark_img)
        # cut the image to two parts
        imag = self.long_slice(rimg, 150)
        for image in imag:
            image = self.add_border(image, border=(10, 50, 10, 60), color='gray')
            word = self.ocrApi.google_ocr(image)
            count += 1
            if count < 1:
                if word is not None:
                    uppercheck.append(word)
            else:
                if word is not None:
                    lowercheck.append(word)
        # check the similar ratio of the item in list
        upper = self.similar(uppercheck)
        lower = self.similar(lowercheck)
        # combine result by difference situation
        if upper is not None and lower is not None:
            dou_line_result = upper + lower
        elif upper is None and lower is not None:
            dou_line_result = lower
        elif lower is None and upper is not None:
            dou_line_result = upper
        if dou_line_result == test:
            dou_line_result = None
        return dou_line_result

    def run_ocr(self, mark_cut_img):
        #q = Queue()
        # ocr_result_singal = Process(target=google_ocr, args=(q, mark_cut_img))
        ocr_result_singal = self.pool.apply_async(self.ocrApi.google_ocr, (mark_cut_img, )).get()
        ocr_result_double = self.pool.apply_async(self.dou_line_ocr, (mark_cut_img, )).get()
        if ocr_result_singal is not None:
            #print("singal : " + ocr_result_singal)
            result = ocr_result_singal
        elif ocr_result_double is not None:
            #print("double : " + ocr_result_double)
            result = ocr_result_double
        else:
            result = ""
        final = result.upper()
        self.parent.result = final
        return final

    def run(self, Oimg, nextCarFront, nextCarBack, label):
        # save the variable
        self.nextCarFront = nextCarFront
        self.nextCarBack = nextCarBack
        self.result = self.run_ocr(Oimg)
        # Logic check for read car plate
        if len(self.result) > 0:
            regr = re.compile("[A-Z0-9]")
            self.result = regr.findall(self.result)
            self.result = "".join(self.result)
            # check the illegal char
            self.result = self.result.replace('I', '1')
            self.result = self.result.replace('Q', '0')
            self.result = self.result.replace('O', '0')
            self.result = self.result.replace("\n", '')
            #print("OCR result: " + self.result)
        # append to result list by the label
        if label == self.parent.license_model_labels[0]:
            self.listresultF.append(self.result)
        elif label == self.parent.license_model_labels[1]:
            self.listresultB.append(self.result)
        # If next car
        if nextCarFront or nextCarBack:
            self.checknextcar()


    # ******************************************Part of checking Answer********************************************

    #next Car
    def checknextcar(self):
        if self.nextCarFront:
            carplatenumF = self.search_correct(self.listresultF)
            self.connentDatabase.add_data_in([carplatenumF, self.listcartype[0]])
            self.listresultF = []
            self.listcartype = []
            self.nextCarFront = False
        if self.nextCarBack:
            carplatenumB = self.search_correct(self.listresultB)
            self.isinpark(carplatenumB)
            self.listresultB = []
            self.nextCarBack = False

    # Search for correct Ans By logic
    def search_correct(self, listresult):
        carplatenum = ""
        eng = ""
        num = ""
        clearlist = self.listPreprocess(listresult)
        # find common word
        most_common_words = self.common_word(clearlist, 3)
        # print("most_common_words")
        # print(most_common_words)
        for word in most_common_words:
            if len(word) == 6:
                carplatenum = word
        while not carplatenum:
            carplatenum = self.alt_da_word(clearlist)
        eng, num = self.find_eng(carplatenum)
        num = num.replace('B', '8')
        eng = eng.replace('2', 'Z')
        carplatenum = eng + num
        return carplatenum


    # delete the nothing result in list
    def listPreprocess(self, list):
        buflist = []
        for i in list:
            if not i == '':
                buflist.append(i)
        return buflist

    def find_eng(self, string):
        string = string.replace(" ", "")
        check_is_int = False
        i = 0
        try:
            while check_is_int== False:
                if re.match('^[0123456789]+$', string[i]):
                    check_is_int = True
                else:
                    check_is_int = False
                    i = i+1
            string2 = string[0:i], string[i:len(string)]
        except:
            string2 = ["",""]
        return string2

    def common_word(self, list, Order):
        return [word for word, word_count in Counter(list).most_common(Order)]

    def alt_da_word(self, clearedlist):
        listeng = []
        listeng2 = []
        listnum = []
        listnum2 = []
        for item in clearedlist:
            eng, num = self.find_eng(item)
            listeng.append(eng)
            listnum.append(num)
        for char in listeng:
            if len(char) == 2:
                listeng2.append(char)
        for num in listnum:
            if len(num) < 5:
                listnum2.append(num)
        sortedwords = sorted(listnum2, key=len)
        num = sortedwords[-1]
        num = num.replace('B', '8')
        if len(listeng2) > 0:
            eng = listeng2[0]
            eng = eng.replace('2', 'Z')
            carnum = eng + num
        elif len(sortedwords) > 0:
            carnum = num
        else:
            carnum = ""
        return carnum

    def isinpark(self, word):
        if word == "":
            return 1
        for index, row in self.connentDatabase.inparking.iterrows():
            sim = SequenceMatcher(None, word, row['Car_Plate']).ratio()
            if sim > 0.7:
                nowtime = datetime.datetime.now()
                staytime = (nowtime-row['In_Time']).total_seconds()/60
                carout = [row['Car_Plate'], nowtime, staytime]
                self.connentDatabase.add_data_out(carout)
                # remove the record of at in park
                self.connentDatabase.drop_data_in(row['Car_Plate'])
                pd = [row['Car_Plate'], row['Car_Type'], row['In_Time'], nowtime, staytime]
                ocrrec = self.parent.addrecord(pd)
                self.connentDatabase.add_data_full(ocrrec)
                self.connentDatabase.printDataFrame()
                self.connentDatabase.save()
                break


if __name__ == '__main__':
    import pandas as pd
    #img = Image.open("C:/Users/Tcc/Desktop/CPR_Model_demo_2/license/166.png")
    img = cv2.imread("C:/Users/Tcc/Desktop/CPR_Model_demo_2/license/166.png")
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    for tnt in range(len(img_gray)):
        for i in range(len(img_gray[tnt])):
            if img_gray[tnt][i] > img_gray[tnt].mean()-35:
                img_gray[tnt][i] = 255
    cv2.imwrite("test.png", img_gray)
    result = tessoract_ocr(img_gray)
    print("result: " + str(result))
