from __future__ import division
from tkinter import *
import tkinter as Tkinter
import cv2

SIZE = 256, 256

CONFIG_PATH = "config.json"
META_DATA_PATH = "meta_data.txt"
IMG_EXT = ["jpg", "png", "jpeg"]
BB_FORMAT = "{:^3} | ({:4}, {:4}) -> ({:4}, {:4})"


class UiController:
    def __init__(self, ui, setting, debugMode):
        self.setting = setting
        self.ui = ui
        self.debugMode = debugMode

    def option_ok(self):
        self.ui.org_privateCarPrice = self.ui.entry_privateCarPrice.get()
        self.ui.org_MediumPrice = self.ui.entry_MediumPrice.get()
        self.ui.org_HGVPrice = self.ui.entry_HGVPrice.get()
        self.ui.org_vanPrice = self.ui.entry_vanPrice.get()
        if self.debugMode:
            print(type(self.ui.org_privateCarPrice))
            print(self.ui.org_privateCarPrice)
            print(self.ui.org_MediumPrice)
            print(self.ui.org_HGVPrice)
            print(self.ui.org_vanPrice)
        self.ui.option_window.destroy()

    def option_cancel(self):
        self.ui.option_window.destroy()

    def moveToBottom(self):
        self.ui.resultCanvas.yview_moveto(1)

    def quit(self):
        self.ui.logic.root.quit()
        self.ui.logic.root.destroy()
