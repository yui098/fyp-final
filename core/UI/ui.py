from __future__ import division
from tkinter import *
from core.UI import UiController
import time

SIZE = 256, 256
RECORD_FORMAT = "%Y-%m-%d %H:%M:%S"
CONFIG_PATH = "config.json"
META_DATA_PATH = "meta_data.txt"
IMG_EXT = ["jpg", "png", "jpeg"]
BB_FORMAT = "{:^3} | ({:4}, {:4}) -> ({:4}, {:4})"


class UI:

    def __init__(self, Logic, setting, debugMode):
        self.setting = setting
        self.logic = Logic
        self.debugMode = debugMode
        self.controller = UiController(self, self.setting, self.debugMode)
        self.org_privateCarPrice = self.setting['price']['privateCar']
        self.org_MediumPrice = self.setting['price']['Medium']
        self.org_HGVPrice = self.setting['price']['HGV']
        self.org_vanPrice = self.setting['price']['Van']

    def start(self, master):
        self.root = master
        self.root.title("CPR_System")
        # Create a canvas that can fit the above video source size
        self.mainframe = Frame(self.root)
        self.mainframe.pack()
        self.leftframe = Frame(self.mainframe)
        self.leftframe.pack(side=LEFT)
        self.canvas = Canvas(self.leftframe, width=self.logic.vid.width, height=self.logic.vid.height)
        self.canvas.pack()
        self.option_button = Button(self.mainframe, text="Option", command=self.option)
        self.option_button.pack(side=RIGHT, fill=X)
        self.exit_btn = Button(self.mainframe, text="Exit", command=self.controller.quit)
        self.exit_btn.pack(side=RIGHT, fill=X)

        self.resultpanel = Frame(self.mainframe)
        self.resultpanel.pack(side=RIGHT, fill=BOTH)
        self.resultLabel = Label(self.resultpanel, text="Record", font='bold')
        self.resultLabel.pack(side=TOP, anchor='nw')
        self.scrollbar = Scrollbar(self.resultpanel, orient=VERTICAL)
        self.scrollbar.pack(side=RIGHT, fill=Y)
        self.resultCanvas = Canvas(self.resultpanel, width=500, yscrollcommand=self.scrollbar.set)
        self.resultCanvas.pack(side=LEFT, fill=BOTH, expand=True)
        self.scrollbar.config(command=self.resultCanvas.yview)
        self.resultCanvas.xview_moveto(0)
        self.resultCanvas.yview_moveto(0)
        self.listPanel = Frame(self.resultCanvas)
        self.scrollPanel = self.resultCanvas.create_window(0, 0, window=self.listPanel, anchor=NW)
        self.listPanel.bind('<Configure>', self.on_configure)

    def option(self):
        self.option_window = Toplevel(self.root)
        self.privateCarPrice = IntVar()
        self.MediumPrice = IntVar()
        self.HGVPrice = IntVar()
        self.vanPrice = IntVar()
        self.option_upper = Frame(self.option_window)
        self.option_upper.pack(side=TOP)
        self.option_main = Frame(self.option_window)
        self.option_main.pack()
        self.privateCarPrice.set(self.org_privateCarPrice)
        self.MediumPrice.set(self.org_MediumPrice)
        self.HGVPrice.set(self.org_HGVPrice)
        self.vanPrice.set(self.org_vanPrice)
        self.entry_privateCarPrice = Entry(self.option_main, textvariable=self.privateCarPrice)
        self.entry_MediumPrice = Entry(self.option_main, textvariable=self.MediumPrice)
        self.entry_HGVPrice = Entry(self.option_main, textvariable=self.HGVPrice)
        self.entry_vanPrice = Entry(self.option_main, textvariable=self.vanPrice)
        self.label_privateCarPrice = Label(self.option_main, text="Private Car Fee")
        self.label_MediumPrice = Label(self.option_main, text="Medium Fee")
        self.label_HGVPrice = Label(self.option_main, text="HGV Fee")
        self.label_vanPrice = Label(self.option_main, text="Van Fee")
        self.label_privateCarPrice.grid(row=0, column=0)
        self.label_MediumPrice.grid(row=1, column=0)
        self.label_HGVPrice.grid(row=2, column=0)
        self.label_vanPrice.grid(row=3, column=0)
        self.entry_privateCarPrice.grid(row=0, column=2)
        self.entry_MediumPrice.grid(row=1, column=2)
        self.entry_HGVPrice.grid(row=2, column=2)
        self.entry_vanPrice.grid(row=3, column=2)
        self.option_lower = Frame(self.option_window)
        self.option_lower.pack(side=BOTTOM)
        self.ok_button = Button(self.option_lower, text="OK", command=self.controller.option_ok)
        self.ok_button.pack(side=LEFT)
        self.cancel_button = Button(self.option_lower, text="Cancel", command=self.controller.option_cancel)
        self.cancel_button.pack(side=RIGHT)

    def on_configure(self, event):
        # update the scrollbars to match the size of the inner frame
        size = (self.listPanel.winfo_reqwidth(), self.listPanel.winfo_reqheight())
        self.resultCanvas.config(scrollregion="0 0 %s %s" % size)
        if self.listPanel.winfo_reqwidth() != self.resultCanvas.winfo_width():
            # update the canvas's width to fit the inner frame
            self.resultCanvas.config(width=self.listPanel.winfo_reqwidth())
    # update scrollregion after starting 'mainloop'
    # when all widgets are in canvas
    # canvas_width = event.width
    # self.listCanvas.itemconfig(self.scrollPanel, width = canvas_width)
    # self.listCanvas.configure(scrollregion=self.listCanvas.bbox('all'))

    def newRecord(self, CarNum ,type , leavetime, staytime, charge):
        itemframe = Frame(self.listPanel, borderwidth=2, relief=SUNKEN)
        itemframe.pack(fill=X)

        CarplateNum = Label(itemframe, text="Car Plate: {}".format(CarNum))
        CarplateNum.grid(row=0, column=0)
        Cartype = Label(itemframe, text="Car Type: {}".format(type))
        Cartype.grid(row=1, column=0)
        leavetime = Label(itemframe, text="Leave time: {}".format(leavetime.strftime(RECORD_FORMAT)))
        leavetime.grid(row=2, column=0)
        parkedtime = Label(itemframe, text="Stay time: {:.2f} (min)".format(staytime))
        parkedtime.grid(row=3, column=0)
        Charge = Label(itemframe, text="Parking Fee: {}".format(charge))
        Charge.grid(row=4, column=0)
        self.controller.moveToBottom()

        return itemframe