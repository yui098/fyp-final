#! /usr/bin/env python

import os, sys
import math, cv2
import numpy as np
import tensorflow as tf
from keras.models import Model
from keras.callbacks import EarlyStopping, ModelCheckpoint, TensorBoard
from keras.layers import Reshape, Conv2D, Input, Lambda
from keras.layers.merge import concatenate
from keras.optimizers import SGD, Adam, RMSprop
from .boundingBox import BoundingBox
from core.Yolo.model import FullYOLO, TinyYOLO, MobileNet, SqueezeNet, InceptionV3, VGG16, ResNet50
from util import sigmoid, softmax, bbox_iou, checkCreate

LOG_PATH = "logs/"


class YOLODetector:
    """
	Create YOLO object detector by selecting the correct base-CNN model
	"""

    def __init__(self, architecture, input_size, labels, max_box_per_image, anchors, showLog=False, debugMode=False):
        self.showLog = showLog
        self.debugMode = debugMode
        self.input_size = input_size
        self.labels = list(labels)
        self.nb_class = len(self.labels)
        self.nb_box = int(len(anchors) / 2)
        self.class_wt = np.ones(self.nb_class, dtype='float32')
        self.anchors = anchors

        self.max_box_per_image = max_box_per_image

        # Create the model by selecting bottom layers
        input_image = Input(shape=(self.input_size, self.input_size, 3))
        self.true_boxes = Input(shape=(1, 1, 1, max_box_per_image, 4))

        if architecture == 'Full Yolo':
            self.feature_extractor = FullYOLO(self.input_size)
        elif architecture == 'Tiny Yolo':
            self.feature_extractor = TinyYOLO(self.input_size)
        elif architecture == 'MobileNet':
            self.feature_extractor = MobileNet(self.input_size)
        elif architecture == 'SqueezeNet':
            self.feature_extractor = SqueezeNet(self.input_size)
        elif architecture == 'InceptionV3':
            self.feature_extractor = InceptionV3(self.input_size)
        elif architecture == 'VGG16':
            self.feature_extractor = VGG16(self.input_size)
        elif architecture == 'ResNet50':
            self.feature_extractor = ResNet50(self.input_size)
        else:
            raise Exception(
                '[Err] Architecture not supported! Only support Full Yolo, Tiny Yolo, MobileNet, SqueezeNet, VGG16, ResNet50, and InceptionV3 at the moment!')

        if self.showLog:
            print("[Info] {}".format(self.feature_extractor.get_output_shape()))
        self.grid_h, self.grid_w = self.feature_extractor.get_output_shape()
        features = self.feature_extractor.extract(input_image)

        # make the object detection layer
        output = Conv2D(self.nb_box * (4 + 1 + self.nb_class),
                        (1, 1), strides=(1, 1),
                        padding='same',
                        name='conv_23',
                        kernel_initializer='lecun_normal')(features)
        output = Reshape((self.grid_h, self.grid_w, self.nb_box, 4 + 1 + self.nb_class))(output)
        output = Lambda(lambda args: args[0])([output, self.true_boxes])

        self.model = Model([input_image, self.true_boxes], output)

        # initialize the weights of the detection layer
        layer = self.model.layers[-4]
        weights = layer.get_weights()

        new_kernel = np.random.normal(size=weights[0].shape) / (self.grid_h * self.grid_w)
        new_bias = np.random.normal(size=weights[1].shape) / (self.grid_h * self.grid_w)

        layer.set_weights([new_kernel, new_bias])

        # print a summary of the whole model
        self.model.summary()
        # check and Create Log folder
        checkCreate(os.path.expanduser(LOG_PATH))

    def load_weights(self, weight_path):
        self.model.load_weights(weight_path)

    # =============================== YOLO Testing Section =====================================
    def predict(self, image):
        image = cv2.resize(image, (self.input_size, self.input_size))
        image = self.feature_extractor.normalize(image)

        input_image = image[:, :, ::-1]
        input_image = np.expand_dims(input_image, 0)
        dummy_array = dummy_array = np.zeros((1, 1, 1, 1, self.max_box_per_image, 4))

        netout = self.model.predict([input_image, dummy_array])[0]
        boxes = self.decode_netout(netout)

        return boxes

    def decode_netout(self, netout, obj_threshold=0.3, nms_threshold=0.3):
        grid_h, grid_w, nb_box = netout.shape[:3]
        boxes = []

        # decode the output by the network
        netout[..., 4] = sigmoid(netout[..., 4])
        netout[..., 5:] = netout[..., 4][..., np.newaxis] * softmax(netout[..., 5:])
        netout[..., 5:] *= netout[..., 5:] > obj_threshold

        for row in range(grid_h):
            for col in range(grid_w):
                for b in range(nb_box):
                    # from 4th element onwards are confidence and class classes
                    classes = netout[row, col, b, 5:]

                    if np.sum(classes) > 0:
                        # first 4 elements are x, y, w, and h
                        x, y, w, h = netout[row, col, b, :4]

                        x = (col + sigmoid(x)) / grid_w  # center position, unit: image width
                        y = (row + sigmoid(y)) / grid_h  # center position, unit: image height
                        w = self.anchors[2 * b + 0] * np.exp(w) / grid_w  # unit: image width
                        h = self.anchors[2 * b + 1] * np.exp(h) / grid_h  # unit: image height
                        confidence = netout[row, col, b, 4]
                        box = BoundingBox(cx=x, cy=y, w=w, h=h, conf=confidence, classes=classes, isCorners=False,
                                          isCoordinates=False)
                        # print(box.ymin,box.xmin,box.ymax,box.xmax)

                        boxes.append(box)

        # suppress non-maximal boxes
        for c in range(self.nb_class):
            sorted_indices = list(reversed(np.argsort([box.classes[c] for box in boxes])))

            for i in range(len(sorted_indices)):
                index_i = sorted_indices[i]

                if boxes[index_i].classes[c] == 0:
                    continue
                else:
                    for j in range(i + 1, len(sorted_indices)):
                        index_j = sorted_indices[j]

                        if bbox_iou(boxes[index_i], boxes[index_j]) >= nms_threshold:
                            boxes[index_j].classes[c] = 0

        # remove the boxes which are less likely than a obj_threshold
        boxes = [box for box in boxes if box.getScore() > obj_threshold]

        return boxes

# =============================== End of YOLO Testing ======================================
