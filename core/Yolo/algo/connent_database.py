import pandas as pd
import os
import datetime
from difflib import SequenceMatcher

__author__ = "Jonathan Tam"
__copyright__ = ""
__credits__ = ["Jonathan Tam", "Ben Ng", "John Po", "Ernest Ng"]
__version__ = "0.0.1"
__maintainer__ = "Jonathan Tam"
__email__ = "tamchungchak@gmail.com"

class connentDatabase:
    def __init__(self, mainocr , ui, database = "parkingdata.csv"):
        self.database = database
        self.mainocr = mainocr
        self.csvpath = os.path.normpath(database)
        try:
            self.parkingdata = pd.read_csv(self.csvpath, index_col=0)
            print("database connect correctly: " + str(self.csvpath))
        except:
            print("database can't connect correctly")
            self.parkingdata = pd.DataFrame(columns=["Car_Plate", "Car_Type", "In_Time", "Out_Time", "Parking_Time", "Fee"])
        self.inparking = pd.DataFrame(columns=["Car_Plate", "Car_Type", "In_Time"])
        self.outparking = pd.DataFrame(columns=["Car_Plate", "Out_Time", "Parking_Time"])
        # only for demo video
        self.add_data_in(["HU6085", "privateCar"])

    def add_data_full(self, data):
        self.parkingdata.loc[len(self.parkingdata)] = data

    def add_data_in(self, data):
        if len(self.inparking.index) > 0:
            beforecplatenum = self.inparking.iat[len(self.inparking.index)-1, 0]
            sim = SequenceMatcher(None, beforecplatenum, data[0]).ratio()
            if sim > 0.7:
                return 1
        data.append(datetime.datetime.now())
        self.inparking.loc[len(self.inparking)] = data
        print(self.inparking)

    def add_data_out(self, data):
        self.outparking.loc[len(self.outparking)] = data

    def drop_data_in(self, carplate):
        self.inparking = self.inparking[self.inparking.Car_Plate != carplate]

    def printDataFrame(self):
        print(self.inparking)
        print(self.outparking)
        print(self.parkingdata)

    def save(self):
        try:
            os.remove(self.database)
        except:
            self.parkingdata.to_csv(self.database)

if __name__ == '__main__':
    cd = connentDatabase("HI","ui")
    print(cd.inparking[cd.inparking.Car_Plate != "HU6085"])
    cd.save()
