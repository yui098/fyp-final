#! /usr/bin/env python

import cv2
from core.UI import UI
from core.Yolo.algo import YOLODetector
from core.Yolo.algo import MyVideoCapture
from core.OCR import Ocr_main
from keras import backend as K
from tkinter import Tk
from PIL import Image, ImageTk
import tkinter
import math
import numpy as np
import threading

__author__ = "Jonathan Tam"
__copyright__ = ""
__credits__ = ["Jonathan Tam", "Ben Ng", "John Po", "Ernest Ng"]
__version__ = "0.0.1"
__maintainer__ = "Jonathan Tam"
__email__ = "tamchungchak@gmail.com"

class carplateDetector:

        def __init__(self, setting, outVideoName="Test1", showLog=False, debugMode=False):
                self.setting = setting
                self.showLog = showLog
                self.debugMode = debugMode
                self.license_model_labels = setting['license_model']['labels']
                self.type_model_labels = setting['type_model']['labels']
                self.video_path = setting['test']['video_path']
                self.database = setting['test']['parking_csv']
                self.api_key = setting['test']['api_key']
                self.outVideoName = outVideoName + ".avi"
                self.vid = MyVideoCapture(self.video_path,  debugMode)
                self.person_class_label = 13 # other
                self.beforeymax = 0
                self.beforeymin = int(self.vid.get_frame()[1].shape[0])
                self.nextCarFront = False
                self.nextCarBack = False
                self.countFrame = 0
                self.countFrameFront = None
                self.countFrameBack = None
                self.waitframe = 40
                self.ratio = 1.
                self.ui = UI(self, setting, debugMode)
                self.ocr = Ocr_main(self, self.database, self.ui, self.api_key, self.debugMode)
                self.weights_path = setting['license_model']['pretrained_weights']
                K.set_learning_phase(0)
                self.license_yolo = YOLODetector(architecture    = setting['license_model']['architecture'],
				input_size          = setting['license_model']['input_size'],
				labels              = self.license_model_labels,
				max_box_per_image   = setting['license_model']['max_box_per_image'],
				anchors             = setting['license_model']['anchors'],
				showLog		    = self.showLog)
                self.license_yolo.load_weights(setting['license_model']['pretrained_weights'])
                self.type_yolo = YOLODetector(architecture    = setting['type_model']['architecture'],
				input_size          = setting['type_model']['input_size'],
				labels              = self.type_model_labels,
				max_box_per_image   = setting['type_model']['max_box_per_image'],
				anchors             = setting['type_model']['anchors'],
				showLog		        = self.showLog)
                self.type_yolo.load_weights(setting['type_model']['pretrained_weights'])
                self.cap = cv2.VideoCapture(self.video_path)
                self.root = Tk()
                self.ui.start(self.root)

        def initUI(self):
                total_frames = self.cap.get(7)
                print("Total frames: %d" % (int(total_frames)))
                fps_ = self.cap.get(5)
                print("FPS: %d" % (int(fps_)))
                #self.stopEvent = threading.Event()
                #self.thread = threading.Thread()
                #self.thread.start()
                self.predictVideo()
                # UI can't close correctly
                self.root.mainloop()

        def predictVideo(self):
                ret, image = self.vid.get_frame()
                self.image = image
                self.license_boxes = self.license_yolo.predict(self.image)
                self.type_boxes = self.type_yolo.predict(self.image)
                # When
                if self.countFrameFront is not None:
                        if self.countFrameFront + self.waitframe == self.countFrame:
                                if self.debugMode:
                                        self.ocr.nextCarFront = True
                if self.countFrameBack is not None:
                        if self.countFrameBack + self.waitframe == self.countFrame:
                                if self.debugMode:
                                        self.ocr.nextCarBack = True
                self.frame = self.draw_boxes(self.image, self.license_boxes, self.license_model_labels, True)
                self.frame = self.draw_boxes(self.frame, self.type_boxes, self.type_model_labels)
                self.frame = cv2.resize(self.image, (int(self.frame.shape[1]/self.ratio), int(self.frame.shape[0]/self.ratio)))
                self.countFrame += 1
                self.delay = 60
                self.photo = ImageTk.PhotoImage(image=Image.fromarray(cv2.cvtColor(self.frame, cv2.COLOR_BGR2RGB)))
                self.ui.canvas.create_image(0, 0, image=self.photo, anchor=tkinter.NW)
                check = threading.Thread(target=self.ocr.checknextcar)
                check.start()
                self.root.after(self.delay, self.predictVideo)

        # Tmp function to draw boxes
        def draw_boxes(self, preimage, boxes, labels, ocr=False):
                self.result = ""
                self.image = preimage
                for box in boxes:
                        # if box.conf <= 0.45:
                        # continue
                        # if box.label != person_class_label:
                        # 	continue
                        self.xmin = int((box.cx - box.w / 2) * self.image.shape[1])
                        self.xmax = int((box.cx + box.w / 2) * self.image.shape[1])
                        self.ymin = int((box.cy - box.h / 2) * self.image.shape[0])
                        self.ymax = int((box.cy + box.h / 2) * self.image.shape[0])
                        # Logic for doing OCR and checking is the car gone
                        if ocr:
                                # cut car plate image
                                self.Oimg = self.image.copy()[self.ymin:self.ymax, self.xmin:self.xmax]
                                # Check car is gone (in car park)
                                if labels[box.getLabel()] == self.license_model_labels[0]:
                                        if self.countFrameFront is None:
                                                self.countFrameFront = self.countFrame
                                        if not self.debugMode:
                                                print(str(self.beforeymax) + "\t" + str(self.ymax))
                                                print(self.beforeymax-2 > self.ymax)
                                        if self.beforeymax-40 > self.ymax and self.countFrame - self.countFrameFront < self.waitframe:
                                                self.nextCarFront = True
                                                self.countFrameFront = self.countFrame + self.waitframe + 1
                                        self.beforeymax = self.ymax
                                        self.countFrameFront = self.countFrame
                                # Check car is gone (Out car park)
                                if labels[box.getLabel()] == self.license_model_labels[1]:
                                        if self.countFrameBack is None:
                                                self.countFrameBack = self.countFrame
                                        if not self.debugMode:
                                                print(str(self.beforeymin) + "\t" + str(self.ymin))
                                                print(self.beforeymin + 2 < self.ymin)
                                        if self.beforeymin+40 < self.ymin and self.countFrame - self.countFrameBack < self.waitframe:
                                                self.nextCarBack = True
                                                self.countFrameBack = self.countFrame + self.waitframe + 1
                                        self.beforeymin = self.ymin
                                        self.countFrameBack = self.countFrame
                                # Do OCR for the car plate image
                                self.trd = threading.Thread(target=self.ocr.run, args=(self.Oimg, self.nextCarFront, self.nextCarBack, labels[box.getLabel()]))
                                self.trd.start()
                                cv2.rectangle(self.image, (self.xmin, self.ymin), (self.xmax, self.ymax), (0, 0, 255), 3)
                                cv2.putText(self.image,
                                            labels[box.getLabel()] + ' ',
                                            (self.xmin, int(self.ymin - 20)),
                                            cv2.FONT_HERSHEY_SIMPLEX,
                                            1e-3 * self.image.shape[0],
                                            (0, 0, 255), 2)
                        else:
                                # When draw car type box
                                self.ocr.listcartype.append(labels[box.getLabel()])
                                cv2.rectangle(self.image, (self.xmin, self.ymin), (self.xmax, self.ymax), (0, 0, 255), 3)
                                cv2.putText(self.image, labels[box.getLabel()],
                                            (self.xmin, int(self.ymin - 20)),
                                            cv2.FONT_HERSHEY_SIMPLEX,
                                            1e-3 * self.image.shape[0],
                                            (0, 0, 255), 2)
                        self.nextCarBack = False
                        self.nextCarFront = False
                return self.image

        def countFee(self, type, time):
                inpark = math.ceil(np.ceil(time)/60)
                _privateCarPrice = int(self.ui.org_privateCarPrice)
                _MediumPrice = int(self.ui.org_MediumPrice)
                _HGVPrice = int(self.ui.org_HGVPrice)
                _vanPrice = int(self.ui.org_vanPrice)
                if type == self.type_model_labels[0]:
                        fee = _privateCarPrice * inpark
                elif type == self.type_model_labels[1]:
                        fee = _MediumPrice * inpark
                elif type == self.type_model_labels[2]:
                        fee = _HGVPrice * inpark
                elif type == self.type_model_labels[3]:
                        fee = _vanPrice * inpark
                else:
                        print("Error Type, Please find for help")
                        fee = None
                return fee

        def addrecord(self, ocrRec):
                charge = self.countFee(ocrRec[1], ocrRec[4])
                ocrRec.append(charge)
                self.ui.newRecord(ocrRec[0], ocrRec[1], ocrRec[3], ocrRec[4], charge)
                return ocrRec
