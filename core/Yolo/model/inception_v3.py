#! /usr/bin/env python

from keras.models import Model
from keras.applications import InceptionV3 as InV3
from keras.layers import Input
from .baseModel import BaseModel

__author__ = "Andy Tsang"
__copyright__ = "Copyright 2017, HungryTech Limited"
__credits__ = ["Andy Tsang"]
__version__ = "1.0.0"
__maintainer__ = "Andy Tsang"
__email__ = "andy.tsang@HungryTech.net"

MODEL_PATH  = "static/model/inception_wo_top.h5"

class InceptionV3(BaseModel):
    """
    InceptionV3 Model
    """
    def __init__(self, input_size):
        input_image = Input(shape=(input_size, input_size, 3))

        inception = InV3(input_shape=(input_size,input_size,3), include_top=False)
        inception.load_weights(MODEL_PATH)

        x = inception(input_image)

        self.feature_extractor = Model(input_image, x)  

    def normalize(self, image):
        image = image / 255.
        image = image - 0.5
        image = image * 2.

        return image