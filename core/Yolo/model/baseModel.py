#! /usr/bin/env python

import tensorflow as tf
from keras.models import Model

__author__ = "Andy Tsang"
__copyright__ = "Copyright 2017, HungryTech Limited"
__credits__ = ["Andy Tsang"]
__version__ = "1.0.0"
__maintainer__ = "Andy Tsang"
__email__ = "andy.tsang@HungryTech.net"

class BaseModel:
	"""
	Base Object for Tensorflow models
	"""
	def __init__(self, input_size):
		raise NotImplementedError("error message")

	def normalize(self, image):
                raise NotImplementedError("error message")
	
	def get_output_shape(self):
		return self.feature_extractor.get_output_shape_at(-1)[1:3]

	def extract(self, input_image):
		return self.feature_extractor(input_image)
