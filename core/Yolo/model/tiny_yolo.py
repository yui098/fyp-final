#! /usr/bin/env python

from keras.models import Model
from keras.layers import Conv2D, Input, MaxPooling2D, BatchNormalization, Lambda
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.merge import concatenate
from .baseModel import BaseModel

__author__ = "Andy Tsang"
__copyright__ = "Copyright 2017, HungryTech Limited"
__credits__ = ["Andy Tsang"]
__version__ = "1.0.0"
__maintainer__ = "Andy Tsang"
__email__ = "andy.tsang@HungryTech.net"

MODEL_PATH  = "static/model/tiny_yolo_wo_top.h5"

class TinyYOLO(BaseModel):
	"""
	Tiny YOLO Model
	"""
	def __init__(self, input_size):
		input_image = Input(shape=(input_size, input_size, 3))

		# Layer 1
		x = Conv2D(16, (3,3), strides=(1,1), padding='same', name='conv_1', use_bias=False)(input_image)
		x = BatchNormalization(name='norm_1')(x)
		x = LeakyReLU(alpha=0.1)(x)
		x = MaxPooling2D(pool_size=(2, 2))(x)

		# Layer 2 - 5
		for i in range(0,4):
			x = Conv2D(32*(2**i), (3,3), strides=(1,1), padding='same', name='conv_' + str(i+2), use_bias=False)(x)
			x = BatchNormalization(name='norm_' + str(i+2))(x)
			x = LeakyReLU(alpha=0.1)(x)
			x = MaxPooling2D(pool_size=(2, 2))(x)

		# Layer 6
		x = Conv2D(512, (3,3), strides=(1,1), padding='same', name='conv_6', use_bias=False)(x)
		x = BatchNormalization(name='norm_6')(x)
		x = LeakyReLU(alpha=0.1)(x)
		x = MaxPooling2D(pool_size=(2, 2), strides=(1,1), padding='same')(x)

		# Layer 7 - 8
		for i in range(0,2):
			x = Conv2D(1024, (3,3), strides=(1,1), padding='same', name='conv_' + str(i+7), use_bias=False)(x)
			x = BatchNormalization(name='norm_' + str(i+7))(x)
			x = LeakyReLU(alpha=0.1)(x)

		self.feature_extractor = Model(input_image, x)
		self.feature_extractor.load_weights(MODEL_PATH)

	def normalize(self, image):
		return image / 255.