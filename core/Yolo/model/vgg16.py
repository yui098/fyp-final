#! /usr/bin/env python

from keras.models import Model
from keras.applications.vgg16 import VGG16 as VG
from .baseModel import BaseModel

__author__ = "Andy Tsang"
__copyright__ = "Copyright 2017, HungryTech Limited"
__credits__ = ["Andy Tsang"]
__version__ = "1.0.0"
__maintainer__ = "Andy Tsang"
__email__ = "andy.tsang@HungryTech.net"

MODEL_PATH  = "static/model/vgg16_wo_top.h5"

class VGG16(BaseModel):
	"""
	VGG16 Model
	"""
	def __init__(self, input_size):
		vgg16 = VG(input_shape=(input_size, input_size, 3), include_top=False)
		vgg16.load_weights(MODEL_PATH)

		self.feature_extractor = vgg16

	def normalize(self, image):
		image = image[..., ::-1]
		image = image.astype('float')

		image[..., 0] -= 103.939
		image[..., 1] -= 116.779
		image[..., 2] -= 123.68

		return image 