#! /usr/bin/env python

import tensorflow as tf
from keras.models import Model
from keras.layers import Reshape, Activation, Conv2D, Input, MaxPooling2D, BatchNormalization, Flatten, Dense, Lambda
from keras.layers.merge import concatenate
from .baseModel import BaseModel

__author__ = "Andy Tsang"
__copyright__ = "Copyright 2017, HungryTech Limited"
__credits__ = ["Andy Tsang"]
__version__ = "1.0.0"
__maintainer__ = "Andy Tsang"
__email__ = "andy.tsang@HungryTech.net"

MODEL_PATH  = "static/model/squeezenet_wo_top.h5"

class SqueezeNet(BaseModel):
	"""
	SqueezeNet
	"""
	def __init__(self, input_size):

		# define some auxiliary variables and the fire module
		sq1x1  = "squeeze1x1"
		exp1x1 = "expand1x1"
		exp3x3 = "expand3x3"
		relu   = "relu_"

		def SqueezeLayer(x, index, squeeze=16, expand=64):
			s_id = 'squeeze' + str(index) + '/'

			x = Conv2D(squeeze, (1, 1), padding='valid', name=s_id + sq1x1)(x)
			x = Activation('relu', name=s_id + relu + sq1x1)(x)

			left = Conv2D(expand,  (1, 1), padding='valid', name=s_id + exp1x1)(x)
			left = Activation('relu', name=s_id + relu + exp1x1)(left)

			right = Conv2D(expand,  (3, 3), padding='same',  name=s_id + exp3x3)(x)
			right = Activation('relu', name=s_id + relu + exp3x3)(right)

			x = concatenate([left, right], axis=3, name=s_id + 'concat')

			return x

		# define the model of SqueezeNet
		input_image = Input(shape=(input_size, input_size, 3))

		x = Conv2D(64, (3, 3), strides=(2, 2), padding='valid', name='conv1')(input_image)
		x = Activation('relu', name='relu_conv1')(x)
		x = MaxPooling2D(pool_size=(3, 3), strides=(2, 2), name='pool1')(x)

		x = SqueezeLayer(x, index=2, squeeze=16, expand=64)
		x = SqueezeLayer(x, index=3, squeeze=16, expand=64)
		x = MaxPooling2D(pool_size=(3, 3), strides=(2, 2), name='pool3')(x)

		x = SqueezeLayer(x, index=4, squeeze=32, expand=128)
		x = SqueezeLayer(x, index=5, squeeze=32, expand=128)
		x = MaxPooling2D(pool_size=(3, 3), strides=(2, 2), name='pool5')(x)

		x = SqueezeLayer(x, index=6, squeeze=48, expand=192)
		x = SqueezeLayer(x, index=7, squeeze=48, expand=192)
		x = SqueezeLayer(x, index=8, squeeze=64, expand=256)
		x = SqueezeLayer(x, index=9, squeeze=64, expand=256)

		self.feature_extractor = Model(input_image, x)  
		self.feature_extractor.load_weights(MODEL_PATH)

	def normalize(self, image):
		image = image[..., ::-1]
		image = image.astype('float')

		image[..., 0] -= 103.939
		image[..., 1] -= 116.779
		image[..., 2] -= 123.68

		return image    