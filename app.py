#! /usr/bin/env python
import time
import argparse, json
from util import basic_args
from core.Yolo.algo import carplateDetector

__author__ = "Jonathan Tam"
__copyright__ = ""
__credits__ = ["Jonathan Tam", "Ben Ng", "John Po", "Ernest Ng"]
__version__ = "0.0.1"
__maintainer__ = "Jonathan Tam"
__email__ = "tamchungchak@gmail.com"

def run(arg):
	setting_path = "setting.json"
	with open(setting_path) as setting_buffer:
		setting = json.loads(setting_buffer.read())
	detector = carplateDetector(setting, "Test1", showLog=True,debugMode=True)
	detector.initUI()
	setting["price"]["privateCar"] = detector.ui.org_privateCarPrice
	setting["price"]["Medium"] = detector.ui.org_MediumPrice
	setting["price"]["HGV"] = detector.ui.org_HGVPrice
	setting["price"]["Van"] = detector.ui.org_vanPrice
	with open(setting_path, 'w') as outfile:
		json.dump(setting, outfile)

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="Car_plate_Reader")
	parser = basic_args(parser)
	parser.add_argument("-p", "--path", type=str, default=None,
						 help="path")

	arg = parser.parse_args()
	st = time.time()
	run(arg)
	print("Finished:{}".format(time.time()-st))
