#! /usr/bin/env python
import argparse

__author__ = "Andy Tsang"
__copyright__ = "Copyright 2018, HungryTech Limited"
__credits__ = ["Andy Tsang"]
__version__ = "1.0.0"
__maintainer__ = "Andy Tsang"
__email__ = "andy.tsang@hungrytech.net"

def str2bool(s):
	"""
	Convert string to Boolean
	Args:
		s (string): string
	Returns:
		boolean (boolean): boolean
	"""
	if s.lower() in ('yes', 'true', 't', 'y', '1'):
		return True
	elif s.lower() in ('no', 'false', 'f', 'n', '0'):
		return False
	else:
		raise argparse.ArgumentTypeError('Boolean value expected.')

def basic_args(parser):
	"""
	Initialize parser with basic arguments.
	Args:
		parser (ArgumentParser): Parser wish to add basic arguments
	Returns:
		parser (ArgumentParser): Parser after modification
	"""
	parser.add_argument("-l", "--showLog", type=str2bool, nargs='?',
						const=True, default=False,
						help="Activate log mode.")
	parser.add_argument("-dbug", "--debugMode", type=str2bool, nargs="?",
						const=True, default=False,
						help="Enable debug mode.")

	return parser