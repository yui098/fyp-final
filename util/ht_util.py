#! /usr/bin/env python
import os,sys
import numpy as np
import cv2,math

__author__ = "Andy Tsang"
__copyright__ = "Copyright 2018, HungryTech Limited"
__credits__ = ["Andy Tsang"]
__version__ = "1.0.0"
__maintainer__ = "Andy Tsang"
__email__ = "andy.tsang@hungrytech.net"

"""
Util function for HungryTech projects
"""

def location2bbox(location):
	"""
	From location [top,right,bot,left] to bbox [x,y,width,height]
	Args:
		location (list): coordinate [top,right,bot,left]
	Returns:
		bbox (list): bounding box [x,y,width,height]
	"""
	bbox = [location[3],location[0],location[1]-location[3],location[2]-location[0]]

	return bbox

def bbox2location(bbox):
	"""
	From bbox [x,y,width,height] to location [top,right,bot,left]
	Args:
		bbox (list): bounding box [x,y,width,height]
	Returns:
		location (list): coordinate [top,right,bot,left]
	"""
	location = [bbox[1],bbox[0]+bbox[2],bbox[1]+bbox[3],bbox[0]]

	return location

def getCenterPoint(bbox):
	"""
	Function to get the center point of a given bbox (top,right,bot,left)
	Args:
		bbox (list): coordinate [top,right,bot,left]
	Returns:
		x (int): x of center
		y (int): y of center
	"""
	width = bbox[1]-bbox[3]
	height = bbox[2]-bbox[0]
	x = bbox[3] + width / 2
	y = bbox[0] + height / 2

	return x,y

def distanceOfpoints(dx,dy,isAbs=True):
	"""
	Function to get distance between points
	Args:
		dx (float): change of x-axis
		dy (float): change of y-axis
		isAbs (boolean): is getting absolute value
	"""
	if isAbs:
		return math.hypot(dx, dy)
	else:
		DoSTH=True

def getTextBoxRatio(text, box_width, text_thickness=1):
	"""
	Return text box ratio 
	Args:
		text (str): string of text wish to print
		box_width (int): Width of the background box
		text_thickness (int): Text thickness 
	Returns:
		text_ratio (float): text ratio for opencv
		nh (int): new height
	"""
	normal_length = cv2.getTextSize(text, cv2.FONT_HERSHEY_DUPLEX, 1, text_thickness)
	tw = normal_length[0][0]
	nh = normal_length[0][1]
	text_ratio = 1
	if tw > box_width:
		text_ratio = (box_width/float(tw))
		nh = cv2.getTextSize(text, cv2.FONT_HERSHEY_DUPLEX, text_ratio, text_thickness)[0][1]

	return text_ratio, nh

class HTQueue():
	"""
	Queue with peek function
	"""
	# https://hg.python.org/cpython/file/2.7/Lib/Queue.py
	def __init__(self, maxsize=0):
		self.maxsize = maxsize
		self.queue = list()

	def peek(self):
		return (np.asarray(self.queue) if not self.empty() else [])

	def qsize(self):
		return len(self.queue)

	def empty(self):
		return len(self.queue) <= 0

	def full(self):
		return len(self.queue) >= self.maxsize

	def put(self, item):
		if self.full():
			self.get()
		self.queue.append(item)

	def get(self):
		if not self.empty():
			value = self.queue[0] 
			for i, item in enumerate(self.queue):
				if i != self.qsize() - 1:
					self.queue[i] = self.queue[i+1]
				else:
					self.queue.pop(i)

			return value
		else:
			return []
