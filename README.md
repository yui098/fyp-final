# Vehicle Registration Marks Detector

## Todo list:
- [x] Add OCR function
- [x] Test OCR script
- [x] Read out the one line Vehicle Registration Marks correctly
- [x] Read out the twice line Vehicle Registration Marks correctly
- [x] Import muititasking method the increase the speed on predict
- [x] Predict twice Yolo model at the same time
- [x] Have a simple UI to show the demo the project
- [ ] increase the OCR accuracy to half of the frame
- [ ] Read out the Personalized Vehicle Registration Marks correctly

## Installation

### 0. Requirement
python 					 3.6.6
pip                      19.0.1

numpy                    1.14.0
pandas                   0.23.3
Pillow                   5.2.0
threaded                 4.0.1
threadpool               1.3.2
Tkinter					 8.6


google-api-core          1.8.1
google-api-python-client 1.7.8
google-auth              1.6.3
google-auth-httplib2     0.0.3
google-cloud-core        0.29.1
google-cloud-storage     1.14.0
google-cloud-vision      0.36.0
google-resumable-media   0.3.2
googleapis-common-protos 1.5.8
googledrivedownloader    0.3

h5py                     2.8.0
matplotlib               2.2.2
imutils                  0.4.6/0.5.2
pytesseract              0.2.6

Keras                    2.1.6
Keras-Applications       1.0.7
Keras-Preprocessing      1.0.92

tensorboard              1.9.0
tensorflow               1.9.0
tensorflow-estimator     1.13.0
tensorflow-gpu           1.9.0

oauth2client             4.1.3
opencv-contrib-python    3.4.2.16
opencv-python            3.4.2.16
openpyxl                 2.6.1

### 1. Example

`python app.py`

## Update log
[05-02-2019 19:00] Finish the google OCR api function
[28-02-2019 15:00] The function can output the frame of car plate and read text by using OCR
[11-03-2019 15:42] Can Read out the one line Vehicle Registration Marks correctly
[15-03-2019 17:06] Added a function to Read out the twice line Vehicle Registration Marks
[18-03-2019 15:42] update the twice line Vehicle Registration Marks function
[25-03-2019 14:38] Function can read out the twice line Vehicle Registration Marks correctly
[11-04-2019 11:30] Using muititasking method to increase the speed on predict
[19-04-2019 17:30] Import muititasking method the increase the speed on predict
[26-04-2019 10:30] Training the car type model
[28-04-2019 18:38] Thread the OCR and the checking part
[05-05-2019 02:50] using TKINTER for build up the mainframe of the UI interface 
[05-05-2019 02:50] Add the UI interface for changing the parking fee
[07-05-2019 23:16] Find the Car Plate Correctly in the ocr result list 
[08-05-2019 22:30] Finish the logic which work if cars come and exit
[09-05-2019 23:30] Finish the logic of cal the car park fee
[11-05-2019 02:50] Open class to save the information of cars
[13-05-2019 03:00] Debug on the logic of the carplate check
[13-05-2019 05:00] Can update the price in the json automaticly

## Remind
Vehicle Registration Marks Checking:
	https://www.dsat.gov.mo/car_new/vehicle_check.aspx?language=c
Personalized Vehicle Registration Marks:
	chrome-extension://oemmndcbldboiebfnladdacbdfmadadm/https://www.td.gov.hk/filemanager/tc/content_4805/pvrm%20guidance%20notes_appendix%201%20(chi).pdf
	(自訂車輛登記號碼組合，須由不超過8個英文字母、數字及／或空位組成。)